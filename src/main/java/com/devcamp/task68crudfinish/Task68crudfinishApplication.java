package com.devcamp.task68crudfinish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task68crudfinishApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task68crudfinishApplication.class, args);
	}

}
